// @ts-check
// Module istandaarden/kroki
// Generate diagrams from simple and intuitive language.
export const name = "istandaarden/kroki";

function u_btoa(buffer) {
  const binary = [];
  const bytes = new Uint8Array(buffer);
  for (let i = 0, il = bytes.byteLength; i < il; i++) {
    binary.push(String.fromCharCode(bytes[i]));
  }
  return btoa(binary.join(""));
}

function textEncode(diagramSource) {
  return new TextEncoder().encode(diagramSource);
}

function compressSource(diagramSource) {
  const data = textEncode(diagramSource);
  const compressed = window.pako.deflate(data, {
    level: 9,
  });
  const b64 = u_btoa(compressed);
  const result = b64.replace(/\+/g, "-").replace(/\//g, "_");
  return result;
}

function makeImageTag(diagramSource) {
  const compressedSource = compressSource(diagramSource);
  console.log(compressedSource);
  const imgElement = document.createElement("img");
  // imgElement.src = `https://plantuml.gitlab-static.net/png/${compressedSource}`;
  imgElement.src = `https://kroki.io/plantuml/png/${compressedSource}`;
  return imgElement;
}

export async function run() {
  const krokiImages = document.querySelectorAll("code");
  if (!krokiImages.length) return;

  krokiImages.forEach(krokiImage => {
    if (krokiImage.className === "plantuml") {
      const parent = krokiImage.parentElement;
      const diagramSource = krokiImage.textContent;
      console.log(diagramSource);
      const image = makeImageTag(diagramSource);
      parent.replaceWith(image);
    }
  });
}
